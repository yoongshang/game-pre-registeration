const emailValidator = require('email-validator');

const countryCode = ['MY', 'SG', 'PH', 'IN', 'TH'];

class Validator {
  static isCountryCodeValid(code) {
    return countryCode.includes(code);
  }

  static isEmailValid(email) {
    return emailValidator.validate(email);
  }

  static isPhoneNumberValidWithCountryCode(phone, code) {
    if (!countryCode.includes(code)) {
      return false;
    }

    let regexPattern = /\d+/;
    switch (code) {
      case 'MY':
        regexPattern = /^60\d{8,12}$/;
        break;
      case 'SG':
        regexPattern = /^65\d{8,12}$/;
        break;
      case 'PH':
        regexPattern = /^63\d{8,12}$/;
        break;
      case 'IN':
        regexPattern = /^62\d{8,12}$/;
        break;
      case 'TH':
        regexPattern = /^66\d{8,12}$/;
        break;
      default: regexPattern = /^\d{8,14}$/;
    }

    return regexPattern.test(phone);
  }

  static isPlatformValid(platform) {
    return ['ios', 'android'].includes(platform);
  }
}

module.exports = Validator;
