
class Helper {
  static randomCharacter(count) {
    if (count.length <= 0) {
      return '';
    }

    const strings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let word = '';
    for (let i = 0; i < count; i += 1) {
      word += strings.charAt(Math.floor(Math.random() * strings.length));
    }

    return word;
  }
}

module.exports = Helper;
