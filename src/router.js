const router = require('express').Router();
const csrfMiddleware = require('csurf')({ cookie: true });
const controller = require('./controllers');

/**
 * WEB
 */
router.get('/', csrfMiddleware, (req, res) => controller.index(req, res));
router.get('/csv', csrfMiddleware, (req, res) => controller.csvPage(req, res));

/**
 * API
 */
router.post('/api/submit', csrfMiddleware, (req, res) => controller.submit(req, res));
router.post('/api/get_result', csrfMiddleware, (req, res) => controller.getResult(req, res));

module.exports = router;
