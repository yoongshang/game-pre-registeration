const path = require('path');
const nodemailer = require('nodemailer');
const _ = require('lodash');
const sequenlizeOperator = require('sequelize').Op;
const model = require('./databases/models');
const mailConfig = require('./configs/app').mail;
const mailParser = require('./mailParser');
const arguments = require('./mailCommandLine');

const User = model.User;

if (!arguments.isValid) {
  return;
}

const transporter = nodemailer.createTransport({
  service: mailConfig.service,
  host: mailConfig.host,
  port: mailConfig.port,
  secure: mailConfig.secure,
  auth: mailConfig.auth,
});

const mailContent = mailParser.module(path.join(__dirname, `../temp/${arguments.templatePath}`));
if (mailContent.subject === '' || mailContent.body === '') {
  console.warn('Invalid Email template : empty subject or empty body file path!');
  return null;
}

const mailOptionsList = {
  test: {
    form: mailConfig.sender,
    bcc: mailConfig.testReceiver,
    subject: mailContent.subject,
    [mailContent.bodyType]: mailContent.body,
    attachments: mailContent.attachments,
  },
  production: {
    form: mailConfig.sender,
    bcc: [],
    subject: mailContent.subject,
    [mailContent.bodyType]: mailContent.body,
    attachments: mailContent.attachments,
  },
};

// mode
if (arguments.isTest) {
  return sendMail(mailOptionsList['test']);
} else {

  return getEmails(arguments.country).then(emailList => {
    console.info(`Country: ${arguments.country}`);
    const emailChunk = _.chunk(emailList, mailConfig.chunkLimit);

    emailChunk.forEach(emails => {
      mailOptionsList['production'].bcc = emails;
      sendMail(mailOptionsList['production']);
    });
  });
}

/**
 * Fetch emails from db.
 * @param {string} mailOption
 */
async function getEmails(country) {

  let emailList = [];
  const results = await User.findAll({
    attributes: ['email'],
    where: {
      email: {
        [sequenlizeOperator.not]: '',
      },
      country: {
        [sequenlizeOperator.in]: country, 
      },
    },
  });
  results.forEach(result => {
    emailList.push(result.dataValues.email);
  });

  return emailList;
}

/**
 * Send Mail
 * @param {object} mailOptions 
 */
function sendMail(mailOptions) {
  console.info(mailOptions);

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.warn(`Testing Email send error: ${err}`);
    } else {
      console.info(`Testing Email sent: ${info.response}`);
    }
  });
}
