const arguments = {
  isValid: true,
  isTest: true,
  templatePath: '',
  country: [],
};

const argv = process.argv;

function thrownErrorCommand(error = 'unknown command!') {
  console.error(`Email Send Fail, ${error}`);
  console.info('Command Example:\n npm run mail :production :path mail-template-20180801.txt :country MY,TH');
  arguments.isValid = false;
}

try {
  if (argv.includes(':test')) {
    arguments.isTest = true;
  }
  if (argv.includes(':production')) {
    arguments.isTest = false;
  }
  if (argv.includes(':path')) {
    arguments.templatePath = argv[argv.indexOf(':path') + 1]
  } else {
    return thrownErrorCommand('invalid command :path!');
  }
  if (argv.includes(':country')) {
    arguments.country = argv[argv.indexOf(':country') + 1].split(',');
  }

  if (argv.includes(':test') && argv.includes(':production')) {
    return thrownErrorCommand('invalid command :test and :production, please choose one of each.');
  }
} catch (err) {
  return thrownErrorCommand();
}

module.exports = arguments;
