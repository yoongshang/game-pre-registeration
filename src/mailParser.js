const path = require('path');
const fs = require('fs');

const TEMP_PATH = path.join(__dirname, '../temp/');

function trimInput(string) {
  return string.trim().replace(/\r\n?\r?\n?/g, '');
}

function parseBodyFile(stringBodyFile) {
  if (stringBodyFile === '') {
    return null;
  }

  return {
    path: path.join(TEMP_PATH, String(stringBodyFile)),
  };
}

function parseAttachmentString(stringAttachments) {
  if (stringAttachments === '') {
    return null;
  }

  const attachmentList = [];
  stringAttachments.split(',').forEach((filePath) => {
    attachmentList.push({
      path: path.join(TEMP_PATH, filePath),
    });
  });

  return attachmentList;
}

function parseEmailContent(filePath) {
  try {
    const rawData = fs.readFileSync(filePath, {
      encoding: 'utf-8',
    });

    const data = rawData.toString('utf-8');

    let bodyType = 'text';
    let bodyTypePattern = /\[BODY:TEXT\]=(.+)/;
    if (data.match(/\[BODY:HTML\]=/)) {
      bodyType = 'html';
      bodyTypePattern = /\[BODY:HTML\]=(.+)/;
    }

    const keywordIndex = {
      subject: data.search(/\[SUBJECT\]=(.+)/),
      body: data.search(bodyTypePattern),
      attachment: data.search(/\[ATTACHEMENT\]=(.*)/),
    };

    const content = {
      bodyType,
      subject: trimInput(data.substring(keywordIndex.subject + 10, keywordIndex.body)),
      body: (keywordIndex.body === -1) ? null : false || parseBodyFile(trimInput(data.substring(keywordIndex.body + 12, keywordIndex.attachment))),
      attachments: (keywordIndex.attachment === -1) ? null : false || parseAttachmentString(trimInput(data.substring(keywordIndex.attachment + 14))),
    };
    return content;
  } catch (err) {
    if (err) {
      console.error(`Read file Error: ${err}`);
    }
  }
  return null;
}

exports.module = parseEmailContent;
