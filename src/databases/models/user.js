module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    platform: {
      type: DataTypes.ENUM('android', 'ios', 'both'),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  }, {
    tableName: 'Users',
    updatedAt: false,
    indexes: [
      {
        unique: true,
        fields: ['phone', 'country'],
        name: 'uniquePhoneAndCountry',
      },
      {
        unique: true,
        fields: ['email'],
        name: 'uniqueEmail',
      }],
  });
  return User;
};
