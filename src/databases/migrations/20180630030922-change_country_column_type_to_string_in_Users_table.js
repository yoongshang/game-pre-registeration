module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.changeColumn('Users', 'country',
    {
      type: Sequelize.STRING(2),
    }),

  down: (queryInterface, Sequelize) => queryInterface.changeColumn('Users', 'country',
    {
      type: Sequelize.TEXT('tiny'),
    }),
};
