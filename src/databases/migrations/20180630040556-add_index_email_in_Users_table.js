module.exports = {
  up: queryInterface => queryInterface.addConstraint('Users',
    ['email'],
    {
      type: 'unique',
      name: 'uniqueEmail',
    }),

  down: queryInterface => queryInterface.removeConstraint('Users', 'uniqueEmail'),
};
