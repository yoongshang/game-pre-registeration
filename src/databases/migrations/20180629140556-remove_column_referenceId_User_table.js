module.exports = {
  up: queryInterface => queryInterface.removeColumn('Users', 'referenceId'),

  down: (queryInterface, Sequelize) => queryInterface.addColumn('Users',
    'referenceId',
    {
      allowNull: false,
      type: Sequelize.INTEGER.UNSIGNED,
    }),
};
