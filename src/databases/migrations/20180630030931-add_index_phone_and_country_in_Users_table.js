module.exports = {
  up: queryInterface => queryInterface.addConstraint('Users',
    ['phone', 'country'],
    {
      type: 'unique',
      name: 'uniquePhoneAndCountry',
    }),

  down: queryInterface => queryInterface.removeConstraint('Users', 'uniquePhoneAndCountry'),
};
