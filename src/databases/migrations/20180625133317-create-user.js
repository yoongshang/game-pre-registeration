module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Users',
    {
      userId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      phone: {
        allowNull: true,
        type: Sequelize.STRING(15),
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      country: {
        allowNull: false,
        type: Sequelize.TEXT('tiny'),
      },
      referenceId: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      platform: {
        allowNull: false,
        type: Sequelize.ENUM('android', 'ios', 'both'),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),

  down: queryInterface => queryInterface.dropTable('Users'),
};
