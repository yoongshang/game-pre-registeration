require('dotenv').config();
const model = require('../databases/models');
const Locales = require('../configs/locale');

const user = model.User;

const getLockIconByCount = (count) => {
  if (count >= 50000) {
    return 'goal-6';
  }
  if (count >= 30000) {
    return 'goal-5';
  }
  if (count >= 20000) {
    return 'goal-4';
  }
  if (count >= 15000) {
    return 'goal-3';
  }
  if (count >= 10000) {
    return 'goal-2';
  }
  if (count >= 5000) {
    return 'goal-1';
  }

  return 'goal-0';
};

/**
 * Index Page
 */
const index = (req, res) => {
  const { lang } = req.query;
  const languagePath = lang;
  const locale = new Locales();

  locale.setLanguageByUri(languagePath);

  const webPageText = locale.loadWebPageText();
  if (webPageText === null) {
    return res.send(null);
  }

  return user.count()
    .then(count => res.render('index', {
      goal: getLockIconByCount(count + parseInt(process.env.FAKE_COUNT, 10)),
      locales: webPageText,
      website: process.env.WEB_SITE,
      availableLanguages: locale.getAvailableLanguages(),
      csrfToken: req.csrfToken(),
      youtubeLink: process.env.YOUTUBE_LINK,
      homeLink: process.env.HOME_LINK,
      facebookLink: process.env.FACEBOOK_LINK,
      iosLink: process.env.IOS_LINK,
      androidLink: process.env.ANDROID_LINK,
    }));
};

/**
 * Csv Download Page
 */
const csvPage = (req, res) => {
  res.render('result', {
    csrfToken: req.csrfToken(),
  });
};

module.exports = {
  index,
  csvPage,
};
