require('dotenv').config();
const fs = require('fs');
const json2csv = require('json2csv').parse;
const validator = require('../validator');
const model = require('../databases/models');
const Locales = require('../configs/locale');
const appConfig = require('../configs/app');

const user = model.User;

/**
 * Submit Api
 * POST: api/submit
 */
const submit = (req, res) => {
  const {
    _csrf,
    language,
    identity,
    country,
    platform,
  } = req.body;
  let findUserObject = {};
  let insertUserObject = {};

  const locale = new Locales();
  locale.setLanguageByIso6391(language);
  const localeText = locale.loadWebPageText();
  if (localeText === null) {
    return res.status(500).send(null);
  }

  if (!validator.isCountryCodeValid(country)) {
    return res.status(400).json({
      result: localeText.api_response.invalid_country,
    });
  }
  if (!validator.isPlatformValid(platform)) {
    return res.status(400).json({
      result: localeText.api_response.invalid_system,
    });
  }
  if (!validator.isEmailValid(identity)) {
    if (!validator.isPhoneNumberValidWithCountryCode(identity, country)) {
      return res.status(400).json({
        result: localeText.api_response.invalid_phone_email,
      });
    }
    findUserObject = {
      phone: identity,
    };
    insertUserObject = {
      phone: identity,
      country,
      platform,
    };
  } else {
    findUserObject = {
      email: identity,
    };
    insertUserObject = {
      email: identity,
      country,
      platform,
    };
  }

  return user.findOrCreate({
    where: findUserObject,
    attributes: ['userId'],
    defaults: insertUserObject,
  }).spread((_, created) => {
    if (created) {
      return res.status(200).json({
        result: localeText.api_response.success,
      });
    }

    return res.status(409).json({
      result: localeText.api_response.invalid_exist,
    });
  }).catch((err) => {
    console.log(err);
    return res.status(500).json({
      result: localeText.api_response.internal_error,
    });
  });
};

/**
 * Get results by download csv file
 * POST: /get_result
 */
const getResult = (req, res) => {
  const { username, password } = req.body;
  if (appConfig.resultDownload.username !== username
      || appConfig.resultDownload.password !== password) {
    res.status(403).send('Wrong username / password!');
  }

  user.findAll()
    .then((results) => {
      const data = results.map(result => result.dataValues);
      const csvFields = ['userId', 'phone', 'email', 'country', 'platform', 'createdAt'];
      const opts = {
        fields: csvFields,
      };

      const path = './temp/result.csv';

      try {
        const csv = json2csv(data, opts);
        fs.writeFile(path, csv, (err) => {
          if (err) {
            console.log(`Result file write error occured: ${err}`);
            res.status(500).send('Result file write error occured, please view log for information.');
          }

          res.download(path);
        });
      } catch (err) {
        console.log(`Result convert error occured: ${err}`);
        res.status(500).send('Result convert error occured, please view log for information.');
      }
    });
};

module.exports = {
  submit,
  getResult,
};
