require('dotenv').config();

const migrationTableName = 'migrations';

module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOSTNAME,
    port: process.env.DB_PORT,
    dialect: process.env.DB_CONNECTION,
    migrationStorageTableName: migrationTableName,
    timezone: process.env.TIMEZONE,
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOSTNAME,
    port: process.env.DB_PORT,
    dialect: process.env.DB_CONNECTION,
    migrationStorageTableName: migrationTableName,
    timezone: process.env.TIMEZONE,
  },
  test: {
    username: '',
    password: '',
    database: '',
    host: '',
    dialect: '',
  },
};
