require('dotenv').config();
const fs = require('fs');
const path = require('path');

let sslKey = '';
let sslCert = '';
if (process.env.HTTPS_ENABLED === 'true') {
  try {
    let sslKey = fs.readFileSync(path.resolve(__dirname, process.env.SSL_KEY_PATH));
    let sslCert = fs.readFileSync(path.resolve(__dirname, process.env.SSL_CERT_PATH));
  } catch (err) {
    console.log(`SSL file read error: ${err.code}`);
  }
}

module.exports = {
  ssl: {
    key: sslKey,
    cert: sslCert,
    requestCert: false,
    rejectUnauthorized: false,
  },
  rateLimit: {
    windowMs: parseInt(process.env.REQUEST_LIMIT_MINUTES, 10) * 60 * 1000 || 1 * 60 * 1000,
    max: parseInt(process.env.REQUEST_LIMIT_MAX, 10) || 15,
    delayMs: 0,
  },
  resultDownload: {
    username: String(process.env.RESULT_DOWNLOAD_USERNAME) || null,
    password: String(process.env.RESULT_DOWNLOAD_PASSWORD) || null,
  },
  mail: {
    service: String(process.env.MAIL_SERVICE) || null,
    host: String(process.env.MAIL_HOST) || 'smtp.example.com',
    port: String(process.env.MAIL_PORT) || 587,
    secure: (process.env.MAIL_SECURE === 'true') || false,
    auth: {
      user: String(process.env.MAIL_AUTH_USERNAME) || '',
      pass: String(process.env.MAIL_AUTH_PASSWORD) || '',
    },
    sender: String(process.env.MAIL_SENDER) || 'mail@example.com',
    testReceiver: String(process.env.MAIL_TEST_RECEIVER) || 'mail-test-receiver@example.com',
    chunkLimit: String(process.env.MAIL_CHUNK_LIMIT) || 100,
  },
};
