require('dotenv').config();
const path = require('path');
const fs = require('fs');

const localesDirectory = path.join(__dirname, '../locales');
const defaultLanguage = 'en';

class Locale {
  constructor() {
    this.english = false;
    this.chinese = false;
    this.indonesia = false;
    this.thailand = false;

    if (process.env.LANGUAGE_ENGLISH === 'true') {
      this.english = true;
    }
    if (process.env.LANGUAGE_CHINESE === 'true') {
      this.chinese = true;
    }
    if (process.env.LANGUAGE_INDONESIA === 'true') {
      this.indonesia = true;
    }
    if (process.env.LANGUAGE_THAILAND === 'true') {
      this.thailand = true;
    }
  }

  setCurrentLanguage(language) {
    if (this.english && language === 'en') {
      this.language = language;
      return;
    }
    if (this.chinese && language === 'zh-cn') {
      this.language = language;
      return;
    }
    if (this.indonesia && language === 'id') {
      this.language = language;
      return;
    }
    if (this.thailand && language === 'th') {
      this.language = language;
      return;
    }

    this.language = defaultLanguage;
  }

  setLanguageByUri(uri) {
    switch (uri) {
      case 'en':
        this.setCurrentLanguage('en');
        break;
      case 'cn':
        this.setCurrentLanguage('zh-cn');
        break;
      case 'id':
        this.setCurrentLanguage('id');
        break;
      case 'th':
        this.setCurrentLanguage('th');
        break;
      default:
        this.setCurrentLanguage(defaultLanguage);
        break;
    }
  }

  setLanguageByIso6391(language) {
    switch (language) {
      case 'en':
        this.setCurrentLanguage('en');
        break;
      case 'zh-Hans':
        this.setCurrentLanguage('zh-cn');
        break;
      case 'id':
        this.setCurrentLanguage('id');
        break;
      case 'th':
        this.setCurrentLanguage('th');
        break;
      default:
        this.setCurrentLanguage(defaultLanguage);
        break;
    }
  }

  getAvailableLanguages() {
    return {
      english: this.english,
      chinese: this.chinese,
      indonesia: this.indonesia,
      thailand: this.thailand,
    };
  }

  loadWebPageText() {
    let localeFile = null;
    let locales = {};

    try {
      localeFile = fs.readFileSync(path.join(localesDirectory, `${this.language}.json`));
    } catch (err) {
      console.log('locales file not found: ', err);
      return null;
    }

    try {
      locales = JSON.parse(localeFile);
    } catch (err) {
      console.log('locales json parse error: ', err);
      return null;
    }

    return locales;
  }
}

module.exports = Locale;
