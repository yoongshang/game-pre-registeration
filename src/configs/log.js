const fs = require('fs');
const path = require('path');
const morgan = require('morgan');

const accessLogStream = fs.createWriteStream(path.join(__dirname, './../../logs/access.log'), { flags: 'a' });
const serverErrorLogStream = fs.createWriteStream(path.join(__dirname, './../../logs/server_error.log'), { flags: 'a' });

// custom datetime format
morgan.token('date', () => (new Date().toISOString()));

const accessLog = morgan('combined', {
  stream: accessLogStream,
  skip: (_, res) => res.statusCode < 400,
});
const serverErrorLog = morgan('combined', {
  stream: serverErrorLogStream,
  skip: (_, res) => res.statusCode < 500,
});

module.exports = {
  accessLog,
  serverErrorLog,
};
