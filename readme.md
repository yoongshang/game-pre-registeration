# 墨香外傳手遊事前登入

## 環境需求
1. 安裝nodejs + npm
2. 安裝mysql 5.7^

## 開始設定
1. 在根目錄下`npm install`會自動安裝需要的套件
2. 復制根目錄下的 `.env.example` > `.env` (復制及命名成.env)
3. 在.env中設定基本port及database相關的資訊
4. 在根目錄下執行`./node_modules/.bin/sequelize db:migrate` 建立資料表
5. 執行`npm start` 開始server。

## 基本說明
 - 使用者資料下載登入頁面： `/csv`
 - 請設定使用者帳戶及密碼才能登入下載。
 - 寄信 Email，請下`npm run mail`。
 - Email功能**不需要**使用`pm2`or`nodemon`在背景執行。
 - https 設定，`HTTPS_ENABLED=true`，設定key與certificate路徑，在專案根目錄下的路徑。

## 更新程式須知
 - 下`npm install`，確保安裝最新的套件。
 - 下`./node_modules/.bin/sequelize db:migrate`，如資料表有異動會被更新。
 - 請檢查`.env.example`，如有新增的環境變數請複製至`.env`

## Mail
  1. 填寫`.env` mail config
    - `MAIL_SERVICE` 請參考 [well-known-mail](https://github.com/nodemailer/nodemailer-wellknown/blob/master/services.json)
    - 如果 `MAIL_SERVICE`有設定，就不需要設定`MAIL_HOST`與`MAIL_PORT`
    - 如欲使用自己的smtp server,不需設定`MAIL_SERVICE`
    - `MAIL_SERVICE` support: gmail, hotmail ...
    - Example: `MAIL_SERVICE=gmail` or `MAIL_SERVICE=hotmail`
  2. 複製 `./example/mail-template.example.txt`樣板 或 `./example/mail-template-html.example.txt` to 檔案名字 Ex: edm-template-2018-08-01.txt。
    - 範例資料夾: `./examples/`
    - Mail 模板(Template)必須包含 SUBJECT, BODY:TEXT/BODY:HTML and ATTACHEMENT **Keywords/關鍵字**, 例如以下範例
    - Mail 模板Template:
      ```
      [SUBJECT]=
      [BODY:TEXT]=
      [ATTACHEMENT]=
      ```
    - Mail 模板範例:
      ```
      [SUBJECT]=我是Txt Email標題
      [BODY:TEXT]=body.txt
      [ATTACHEMENT]=something.pdf,edm-2018.pdf
      ```
      或
      ```
      [SUBJECT]=我是Html Email標題
      [BODY:HTML]=body.html
      [ATTACHEMENT]=
      ```
    - Mail body 必須輸入在Temp資料夾下的檔案名字，例如`./Temp/body.txt` 及 `./Temp/body.html`，請輸入`[BODY:TEXT]=body.txt` 或 `[BODY:HTML]=body.html`
    - Mail Attachement必須輸入在Temp資料夾下的檔案名字，例如`./Temp/edm-June-CN.pdf` 及 `./Temp/edm-July-CN.pdf`，請輸入`[ATTACHEMENT]=edm-June-CN.pdf,edm-July-CN.pdf`，`,`逗點後不需要空白!
  3. Mail 執行指令(Command): `npm run mail :test/:production :path {mail template filename in temp folder} :country MY,IN,SG,PH,TH`
    - 指令分為幾個部分:
      - 測試模式 及 正式發送模式 : `:test` 及 `:production`
      - 模板路徑 `:path {mail template filename in temp folder}`，檔案必須放在`./Temp`資料夾下。
      - 選擇國家，`:country {Country Code}`，選項為`MY`馬來西亞, `IN`印尼, `SG`新加坡, `PH`菲律賓；可以多選，例如：`:country MY,IN,SG`
    - `{mail template filename in temp folder}`為 `Temp`資料夾下的模板名稱。
    - 執行Mail指令範例:
      - 執行測試(Test)模式, `npm run mail :test :path edm-template-2018-01.txt`
      - 執行正式發送模式，及傳送給馬來西亞(Malaysia)及印尼(Indonesia), `npm run mail :production :path edm-template-2018-08-01.txt :country MY,IN`

## environment環境變數說明
```
NODE_ENV=development  //環境，部署時改成production
NODE_PORT=3000        //server start port號
TIMEZONE=Asia/Taipei  //時區

HTTPS_ENABLED=true
SSL_KEY_PATH=ssl.key      // ssl key 檔案路徑
SSL_CERT_PATH=ssl.cert    // ssl cert 檔案路徑

REQUEST_LIMIT_MINUTES=1 //限制送出submit的時間
REQUEST_LIMIT_MAX=15    //在時間內送出submit的次數，以這個範例：1分鍾內最多送出15個請求。

DB_CONNECTION=mysql   //目前只支援mysql 
DB_HOSTNAME=localhost //mysql host name
DB_PORT=3306          //mysql port號
DB_DATABASE=database  //database名字
DB_USERNAME=root      //mysql username
DB_PASSWORD=root      //mysql password

WEB_SITE=             //網址，domain name
FAKE_COUNT=0          //登入次數假資料、畫面會顯示目前真實總量加上此假資料的數量

// 語言開關
LANGUAGE_CHINESE=true     
LANGUAGE_ENGLISH=true
LANGUAGE_INDONESIA=true
LANGUAGE_THAILAND=true

//登入下載使用者資料
RESULT_DOWNLOAD_USERNAME=admin123456      //帳戶
RESULT_DOWNLOAD_PASSWORD=asdfghjkl123456  //密碼

// Email
MAIL_SERVICE=                             // mail wellknown service, gmail, hotmail ..etc
MAIL_HOST=                                // mail server host
MAIL_PORT=                                // mail server port, default 587 
MAIL_AUTH_USERNAME=                       // mail authorization username
MAIL_AUTH_PASSWORD=                       // mail authorization username
MAIL_SENDER=                              // mail sender email address
MAIL_TEST_RECEIVER=                       // testing receiver email address
MAIL_CHUNK_LIMIT=                         // 批量寄送限制數量,默認100封

// 頁面連結設定
YOUTUBE_LINK=https://www.youtube.com/embed/UgZePjbXEIM          // Youtube「嵌入」連結
FACEBOOK_LINK=https://www.facebook.com/RebelOfRanger            // facebook 連結
HOME_LINK=http://ror.nuggetfun.com/                             // 首頁連結
IOS_LINK=http://ror.nuggetfun.com/                              // ios 下載連結
ANDROID_LINK=http://ror.nuggetfun.com/                          // android 下載連結
```
