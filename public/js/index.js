'use strict';

$(function () {
  $('img').on('dragstart', function (event) {
    event.preventDefault();
  });
  $('form').submit(function (event) {
    event.preventDefault();

    if ($('.agree-input').is(':checked') === false) {
      swal({
        text: privacy_unchecked
      });
      return;
    }

    var csrfToken = $(event.target).find('[name="_csrf"]').val();
    var language = $('html').attr('lang');
    var identity = $(event.target).find('[name="identity"]').val();
    var country = $(event.target).find('[name="country"]').val();
    var platform = $(event.target).find('[name="platform"]').val();

    $.ajax({
      url: '/api/submit',
      type: 'POST',
      data: JSON.stringify({
        _csrf: csrfToken,
        language: language,
        identity: identity,
        country: country,
        platform: platform
      }),
      dataType: 'json',
      contentType: 'application/json',
      success: function success(json) {
        swal(json.result);
        $('form')[0].reset();
      },
      error: function error(jqXHR) {
        swal(jqXHR.responseJSON.result);
      }
    });
  });
  $('.privacy').click(function () {
    swal({
      html: '<iframe src="http://www.nuggetfun.com/privacy.html" width="100%" height="300"  frameborder="0">Privacy</iframe>',
      customClass: 'privacy-modal'
    });
  });
  $('.lang-slickNav').slicknav({
    label: ''
  });

  $('.login-title .form-text').fitText(0.8);
  $('[lang="th"] .login-title .form-text').fitText(0.6);

  $('.like-icon-wrap .form-text').fitText(1.4);
  $('.system-input-wrap .form-text').fitText(1.4);
  $('.mail-input-wrap .form-text').fitText(1.4);
  $('[lang="en"] .like-icon-wrap .form-text').fitText(1.6);
  $('[lang="en"] .system-input-wrap .form-text').fitText(1.6);
  $('[lang="en"] .mail-input-wrap .form-text').fitText(1.6);
  $('[lang="th"] .like-icon-wrap .form-text').fitText(1.7);
  $('[lang="th"] .system-input-wrap .form-text').fitText(1.7);
  $('[lang="th"] .mail-input-wrap .form-text').fitText(1.7);

  $('.agree-input-wrap').fitText(1.4);
  $('[lang="en"] .agree-input-wrap').fitText(1.7);
  $('[lang="th"] .agree-input-wrap').fitText(1.7);
});