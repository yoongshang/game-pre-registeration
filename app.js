const dotenv = require('dotenv').config();

if (dotenv.error) {
  throw dotenv.error;
}

const express = require('express');
const https = require('https');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const RateLimit = require('express-rate-limit');

const app = express();
const compression = require('compression');
const config = require('./src/configs/app');
const db = require('./src/databases/models');
const { accessLog, serverErrorLog } = require('./src/configs/log');
const router = require('./src/router');

db.sequelize
  .authenticate()
  .then(() => console.log('Database connection has been established successfully.'))
  .catch(err => console.log('Unable to connect to database: ', err));

const limiter = new RateLimit(config.rateLimit);

// config
app.use(express.static('public'));
app.set('views', './src/views');
app.set('view engine', 'pug');
app.use(accessLog);
app.use(serverErrorLog);
app.use(helmet());
app.use(cookieParser());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/', limiter);
app.use(router);

// handle csrf error
app.use((err, _, res, next) => {
  if (err.code !== 'EBADCSRFTOKEN') return next(err);

  return res.status(403).json({
    result: 'Invalid Operation',
  });
});

// server
if (process.env.HTTPS_ENABLED === 'true') {
  https.createServer(config.ssl, app).listen(process.env.NODE_PORT, () => {
    console.log(`Server Start on HTTPS, port: ${process.env.NODE_PORT}`);
  });
} else {
  app.listen(process.env.NODE_PORT, () => {
    console.log(`Server Start on HTTP, port: ${process.env.NODE_PORT}`);
  });
}
